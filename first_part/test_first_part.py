from src import exercise_one

test_output = "1\n2\nThree\n4\nFive\nThree\n7\n8\nThree\nFive\n11\nThree\n13\n14\nThreeFive\n16\n17\nThree\n19\nFive\nThree\n22\n23\nThree\nFive\n26\nThree\n28\n29\nThreeFive\n31\n32\nThree\n34\nFive\nThree\n37\n38\nThree\nFive\n41\nThree\n43\n44\nThreeFive\n46\n47\nThree\n49\nFive\nThree\n52\n53\nThree\nFive\n56\nThree\n58\n59\nThreeFive\n61\n62\nThree\n64\nFive\nThree\n67\n68\nThree\nFive\n71\nThree\n73\n74\nThreeFive\n76\n77\nThree\n79\nFive\nThree\n82\n83\nThree\nFive\n86\nThree\n88\n89\nThreeFive\n91\n92\nThree\n94\nFive\nThree\n97\n98\nThree\nFive\n"


def test_first_exercise(capsys):
    exercise_one()
    captured = capsys.readouterr()
    assert captured.out == test_output


#EXERCISE 2:
def is_colorful(number):
    # Convert the number to a string to work with its digits
    number_str = str(number)
    length = len(number_str)
    products = set()

    # Generate all possible consecutive subsets and calculate their product
    for i in range(length):
        for j in range(i + 1, length + 1):
            subset = number_str[i:j]
            product = 1
            for digit in subset:
                product *= int(digit)
            if product in products:
                return False  # If a product is repeated, it's not colorful
            products.add(product)

    return True  # If all products are different, it's colorful

# Test cases
print(is_colorful(263))   # True
print(is_colorful(236))   # False
print(is_colorful(2532))  


#EXERCISE 3:
def calculate(lst):
    total = 0

    for item in lst:
        if isinstance(item, str) and item.isdigit():
            total += int(item)

    if total == 0:
        return False

    return total

# Test cases
print(calculate(['4', '3', '-2']))  # ➞ 5
print(calculate([453]))             # ➞ False
print(calculate(['nothing', 3, '8', 2, '1']))  # ➞ 9
print(calculate('54'))              # ➞ False

#EXERCISE 4:
def anagrams(word, words):
    sorted_word = sorted(word)
    return [w for w in words if sorted(w) == sorted_word]

# Test cases
print(anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']))  # ['aabb', 'bbaa']
print(anagrams('racer', ['crazer', 'carer', 'racar', 'caers', 'racer']))  # ['carer', 'racer']
print(anagrams('laser', ['lazing', 'lazy',  'lacer']))  # []
