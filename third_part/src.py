#1
import requests

def http_request():
    url = "https://httpbin.org/anything"
    data = {"isadmin": "1"}
    headers = {
        "User-Agent": "MyCustomUserAgent"  # Change this to your desired user-agent string
    }

    try:
        response = requests.post(url, data=data,headers=headers)
        response.raise_for_status()  # Raise an exception if the request was unsuccessful (e.g., 4xx or 5xx response)
        return response.text  # Return the response body as a string
    except requests.exceptions.RequestException as e:
        # Handle any exceptions that may occur during the request
        print(f"An error occurred: {e}")
        return None

# Example usage:
response_body = http_request()
if response_body is not None:
    print(response_body)
#2
import scrapy

class WoolworthsItem(scrapy.Item):
    product_name = scrapy.Field()
    breadcrumb = scrapy.Field()

class WoolworthsSpider(scrapy.Spider):
    name = "woolworths"
    start_urls = ["https://www.woolworths.com.au/shop/browse/drinks/cordials-juices-iced-teas/iced-teas"]

    def parse(self, response):
        # Extract breadcrumb
        breadcrumb = response.css(".ais-BreadcrumbItem-root::text").extract()

        # Extract product names
        product_names = response.css(".ais-ProductGridItem-title::text").extract()

        # Create items
        for product_name in product_names:
            item = WoolworthsItem()
            item["product_name"] = product_name.strip()
            item["breadcrumb"] = breadcrumb
            yield item
#3
import json
import gzip
import csv
import logging

class ProductProcessor:
    def __init__(self, input_file_path, output_file_path):
        self.input_file_path = input_file_path
        self.output_file_path = output_file_path
        self.logger = self.setup_logger()

    def setup_logger(self):
        logger = logging.getLogger("ProductProcessor")
        logger.setLevel(logging.DEBUG)
        
        # Create a console handler
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        
        # Create a file handler
        fh = logging.FileHandler("product_processor.log")
        fh.setLevel(logging.ERROR)
        
        # Create a formatter and set it for the handlers
        formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        ch.setFormatter(formatter)
        fh.setFormatter(formatter)
        
        # Add the handlers to the logger
        logger.addHandler(ch)
        logger.addHandler(fh)
        
        return logger

    def process_products(self):
        print("work")
        try:
            with gzip.open(self.input_file_path, 'rt') as file:
                data = json.load(file)
                available_products = []

                if 'category' in data:
                    breadcrumb = data['category'].split(" > ")
                else:
                    breadcrumb = ["Unknown"]

                for product in data.get('products', []):
                    product_name = product.get('name', '')
                    product_id = product.get('id', '')
                    product_price = product.get('price', '')
                    product_available = product.get('available', False)

                    if product_name:
                        truncated_product_name = product_name[:30]
                    else:
                        truncated_product_name = "Unknown"

                    if isinstance(product_price, (int, float)):
                        rounded_price = round(product_price, 1)
                    else:
                        rounded_price = "Unknown"

                    if product_available:
                        available_products.append((truncated_product_name, rounded_price))
                    else:
                        self.logger.warning(f"Product unavailable - ID: {product_id}, Name: {product_name}")

                if available_products:
                    self.save_to_csv(breadcrumb, available_products)
                else:
                    self.logger.error("No available products found.")

        except Exception as e:
            self.logger.error(f"Error processing products: {str(e)}")

    def save_to_csv(self, breadcrumb, available_products):
        try:
            with open(self.output_file_path, 'w', newline='') as csvfile:
                csv_writer = csv.writer(csvfile)
                csv_writer.writerow(["Breadcrumb", "Product Name", "Product Price"])
                for product_name, product_price in available_products:
                    csv_writer.writerow([">".join(breadcrumb), product_name, product_price])
        except Exception as e:
            self.logger.error(f"Error saving to CSV: {str(e)}")

if __name__ == "__main__":
    input_file_path = "/home/uma/Videos/test_python_scraping/third_part/data/data.json.gz"
    output_file_path = "available_products.csv"

    product_processor = ProductProcessor(input_file_path, output_file_path)
    product_processor.process_products()


