#1
import random
def random_gen():
    while True:
        random_number = random.randint(10, 20)
        yield random_number
        if random_number == 15:
            break
for number in random_gen():
    print(number)

#2
def decorator_to_str(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        return str(result)  # Convert the result to a string before returning it
    return wrapper

#3
@decorator_to_str
def add(a, b):
    return a + b


@decorator_to_str
def get_info(name, age):
    return f"Name: {name}, Age: {age}"

# Testing the decorated functions
result1 = add(5, 3)
result2 = get_info("Alice", 30)

print(result1)  # "8"
print(result2)  # "Name: Alice, Age: 30"

#3
def ignore_exception(exception):
    def decorator(func):
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except exception:
                return None
        return wrapper
    return decorator

@ignore_exception(ZeroDivisionError)
def div(a, b):
    return a / b


@ignore_exception(ValueError)
def parse_int(s):
    return int(s)

result1 = parse_int("123")  # Returns 123
result2 = parse_int("abc")  # Returns None

print(result1)
print(result2)


# exercise 4
class CacheDecorator:
    def __init__(self, func):
        self.func = func
        self.cache = {}

    def __call__(self, *args):
        if args in self.cache:
            return self.cache[args]
        result = self.func(*args)
        self.cache[args] = result
        return result
import unittest

# Assuming you have the CacheDecorator class here...

class TestCacheDecorator(unittest.TestCase):
    def test_cache_decorator_basic(self):
        # Test the basic functionality of caching
        @CacheDecorator
        def add(a, b):
            return a + b

        result1 = add(2, 3)
        result2 = add(2, 3)
        
        self.assertEqual(result1, result2)  # Should pass

    def test_cache_decorator_mutable_arguments(self):
        # Test with mutable arguments (lists)
        @CacheDecorator
        def reverse_list(lst):
            return lst[::-1]

        original_list = [1, 2, 3]
        result1 = reverse_list(original_list)
        original_list.append(4)  # Modify the original list
        result2 = reverse_list(original_list)
        
        self.assertNotEqual(result1, result2)  # Should fail due to mutable arguments

    def test_cache_decorator_kwargs(self):
        # Test with keyword arguments
        @CacheDecorator
        def multiply(a, b=1):
            return a * b

        result1 = multiply(2)
        result2 = multiply(2)
        
        self.assertEqual(result1, result2)  # Should pass

if __name__ == '__main__':
    unittest.main()

